#include "parser.hpp"

#include "grammar.h"
#include "stdlib.h"

extern void* ParseAlloc(void* (*mallocProc)(size_t));
extern void ParseFree(void* p, void (*freeProce)(void*));
extern void ParseTrace(FILE* trace_file, char* trace_prompt);
extern void Parse(void* p,
                  int token_num,
                  const Token* token,
                  ParserParams* params);
#define conv_token(tok) \
    case TOK_##tok:     \
        return PTOK_##tok

int parser_conv_token(TokenType t) {
    switch(t) {
        conv_token(IDENTIFIER);
        conv_token(BOOL);
        conv_token(INTEGER);
        conv_token(FLOAT);
        conv_token(STRING);

        conv_token(OP_ASSIGN);
        conv_token(OP_EQ);
        conv_token(OP_NOTEQ);
        conv_token(OP_LESS);
        conv_token(OP_LESSEQ);
        conv_token(OP_MORE);
        conv_token(OP_MOREEQ);
        conv_token(OP_PLUS);
        conv_token(OP_MINUS);
        conv_token(OP_MUL);
        conv_token(OP_DIV);
        conv_token(OP_PLUSASSIGN);
        conv_token(OP_MINUSASSIGN);
        conv_token(OP_MULASSIGN);
        conv_token(OP_DIVASSIGN);

        conv_token(KEY_VAR);
        conv_token(KEY_FUNC);
        conv_token(KEY_RETURN);
        conv_token(KEY_IF);
        conv_token(KEY_ELSE);
        conv_token(KEY_FOR);

        conv_token(SYN_OPEN_PAREN);
        conv_token(SYN_CLOSE_PAREN);
        conv_token(SYN_OPEN_BRACE);
        conv_token(SYN_CLOSE_BRACE);
        conv_token(SYN_OPEN_BRACKET);
        conv_token(SYN_CLOSE_BRACKET);
        conv_token(SYN_COMMA);
        conv_token(SYN_SEMICOLON);
        conv_token(SYN_COLON);
        conv_token(SYN_DOT);
        default:
            return -1;
    }
}

Error parser_parse(const std::vector<Token>& in,
                   Node** out,
                   LinAlloc* allocator,
                   const char* file) {
    assert(out);
    assert(allocator);

    ParserParams params = {file, allocator, nullptr, Error()};

    void* p = ParseAlloc(malloc);
    for(size_t i = 0; i < in.size(); ++i) {
        const Token* tok = &in[i];
        Parse(p, parser_conv_token(tok->type), tok, &params);
        if(!params.error.success()) {
            // Early-out on first encountered error
            ParseFree(p, free);
            return params.error;
        }
    }
    Parse(p, 0, nullptr, &params);
    ParseFree(p, free);

    *out = params.out;

    return params.error;
}
