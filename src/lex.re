#include "lex.hpp"

#include <string.h>
#include <stdio.h>

bool Token::parse_bool(bool& out) const {
    if(str == "true") {
        out = true;
        return true;
    }
    if(str == "false") {
        out = false;
        return true;
    }
    return false;
}

bool Token::parse_int(int64& out) const {
    int64 scanned;

    bool is_bin = false;
    bool is_hex = false;

    if(str.len > 2) {
        is_bin = str.startswith("0b");
        is_hex = str.startswith("0x");
        assert(!(is_bin && is_hex));
    }

    if(is_bin) {
        size_t idx = str.len-1;
        int64 base = 1;
        int64 sum = 0;
        while(str.ptr[idx] != 'b') {
            int64 digit = int64(str.ptr[idx] - '0');
            sum += base * digit;
            --idx;
            base *= 2;
        }   
        out = sum;
        return true;
    }
    else if(is_hex) {
        if(sscanf(str.ptr, "%llx", &scanned) == 1) {
            out = scanned;
            return true;
        }
    }
    else {
        if(sscanf(str.ptr, "%lld", &scanned) == 1) {
            out = scanned;
            return true;
        }
    }
    return false;
}

bool Token::parse_uint(uint64& out) const {
    uint64 scanned;
    if(sscanf(str.ptr, "%llu", &scanned) == 1) {
        out = scanned;
        return true;
    }
    return false;
}

bool Token::parse_float(float& out) const {
    float scanned;
    if(sscanf(str.ptr, "%f", &scanned) == 1) {
        out = scanned;
        return true;
    }
    return false;
}

bool Token::parse_double(double& out) const {
    double scanned;
    if(sscanf(str.ptr, "%lf", &scanned) == 1) {
        out = scanned;
        return true;
    }
    return false;
}

bool Token::parse_string(std::string& out) const {
    if(str.len >= 2) {
        if(str.ptr[0] == '"' && str.ptr[str.len-1] == '"') {
            out = std::string(&str.ptr[1], str.len-2);
            return true;
        }
    }

    return false;
}

Token::Token(TokenType type, const Str& str)
    : type(type), str(str), line(0), column(0), file(nullptr) {}

Token::Token(TokenType type, const char* ts, const char* te,
    int line, int column, const char* file)
    : type(type), str(ts, te), line(line), column(column), file(file) {}

#define ADD(type) {out.push_back(Token(type, tok, YYCURSOR, line, column, file)); continue;}
#define ERR(t, msg) {error.set(t, line, column, file, msg); return error;}

Error lex_scan(const char* in, const char* file, std::vector<Token>& out) {
    const char* YYCURSOR = in;
    const char* YYLIMIT = in + strlen(in);
    const char* YYMARKER;

    const char* lc_cursor = in;
    int line = 1;
    int column = 0;

    Error error;

    for(;;) {
        const char* tok = YYCURSOR;
        if(tok >= YYLIMIT) break;

        // Count lines and columns
        while(lc_cursor < tok) {
            if(*lc_cursor == '\n') {
                line++;
                column = 0;
            } else {
                column++;
            }
            lc_cursor++;
        }

        /*!re2c
            re2c:define:YYCTYPE = char;
            re2c:yyfill:enable = 0;

            end = "\x00";

            *   { ERR(ERR_LEX_UNEXPECTED_INPUT, "unexpected lexer input"); }
            end { return error; }

            //newline = "\n" | "\r\n";
            //newline { line++; continue; }

            // Whitespaces, comments
            mcm = "/*" ([^*\x00] | ("*" [^/\x00]))* "*""/";
            mcm { continue; }
            badmcm = "/*" ([^*\x00] | ("*" [^/\x00]))* ;
            badmcm { ERR(ERR_LEX_UNCLOSED_COMMENT, "unclosed multiline comment"); }
            scm = "//" [^\n\x00]* ("\n" | end);
            scm { continue; }
            wsp = ([ \t\v\n\r])+;
            wsp { continue; }

            // Strings
            str = "\"" [^\n\"\x00]* "\"";
            str { ADD(TOK_STRING); }
            badstr = "\"" [^\"\n\x00]* ("\n" | end);
            badstr { ERR(ERR_LEX_UNCLOSED_STRING, "unclosed string literal"); }

            // Integer
            hex = "0x"[0-9A-Fa-f]+;
            dec = "-"? [0-9]+;
            bin = "0b"[01]+;
            int = (dec | bin | hex);
            int { ADD(TOK_INTEGER); }

            // Float
            frc = [0-9]* "." [0-9]+ | [0-9]+ ".";
            exp = 'e' [+-]? [0-9]+;
            flt = (frc exp? | [0-9]+ exp) [fFlL]?;
            flt { ADD(TOK_FLOAT); }

            // Bool
            "true"      { ADD(TOK_BOOL); }
            "false"     { ADD(TOK_BOOL); }

            // Keywords
            "func"      { ADD(TOK_KEY_FUNC); }
            "struct"    { ADD(TOK_KEY_STRUCT); }
            "return"    { ADD(TOK_KEY_RETURN); }
            "goto"      { ADD(TOK_KEY_GOTO); }
            "for"       { ADD(TOK_KEY_FOR); }
            "while"     { ADD(TOK_KEY_WHILE); }
            "if"        { ADD(TOK_KEY_IF); }
            "else"      { ADD(TOK_KEY_ELSE); }
            "var"       { ADD(TOK_KEY_VAR); }
            "const"     { ADD(TOK_KEY_CONST); }
            "module"    { ADD(TOK_KEY_MODULE); }
            "use"       { ADD(TOK_KEY_USE); }

            // Operators
            "="         { ADD(TOK_OP_ASSIGN); }
            "=="        { ADD(TOK_OP_EQ); }
            "!="        { ADD(TOK_OP_NOTEQ); }
            "<"         { ADD(TOK_OP_LESS); }
            "<="        { ADD(TOK_OP_LESSEQ); }
            ">"         { ADD(TOK_OP_MORE); }
            ">="        { ADD(TOK_OP_MOREEQ); }
            "+"         { ADD(TOK_OP_PLUS); }
            "-"         { ADD(TOK_OP_MINUS); }
            "*"         { ADD(TOK_OP_MUL); }
            "/"         { ADD(TOK_OP_DIV); }
            "+="        { ADD(TOK_OP_PLUSASSIGN); }
            "-="        { ADD(TOK_OP_MINUSASSIGN); }
            "*="        { ADD(TOK_OP_MULASSIGN); }
            "/="        { ADD(TOK_OP_DIVASSIGN); }
            "++"        { ADD(TOK_OP_PLUSPLUS); }
            "--"        { ADD(TOK_OP_MINUSMINUS); }

            // Misc. syntax
            "{"         { ADD(TOK_SYN_OPEN_BRACE); }
            "}"         { ADD(TOK_SYN_CLOSE_BRACE); }
            "("         { ADD(TOK_SYN_OPEN_PAREN); }
            ")"         { ADD(TOK_SYN_CLOSE_PAREN); }
            "["         { ADD(TOK_SYN_OPEN_BRACKET); }
            "]"         { ADD(TOK_SYN_CLOSE_BRACKET); }
            "."         { ADD(TOK_SYN_DOT); }
            ","         { ADD(TOK_SYN_COMMA); }
            ":"         { ADD(TOK_SYN_COLON); }
            ";"         { ADD(TOK_SYN_SEMICOLON); }

            // Identifiers
            id = [a-zA-Z_][a-zA-Z_0-9]*;
            id { ADD(TOK_IDENTIFIER); }
        */
    }

    return error;
}
