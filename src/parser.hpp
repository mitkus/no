#pragma once

#include <vector>

#include "ast.hpp"
#include "common.hpp"
#include "error.hpp"
#include "lex.hpp"
#include "linalloc.hpp"

struct ParserParams {
    const char* file;
    LinAlloc* allocator;
    Node* out;
    Error error;
};

int parser_conv_token(TokenType t);
Error parser_parse(const std::vector<Token>& in,
                   Node** out,
                   LinAlloc* allocator,
                   const char* file = "");
