#pragma once

#include "common.hpp"
#include "lex.hpp"
#include "linalloc.hpp"
#include "strutil.hpp"

static const char* indent() { return "    "; }

struct SimpleType;
struct ArrayType;
struct FuncType;
struct Block;
struct Decl;
struct ForStmt;
struct IfStmt;
struct ReturnStmt;
struct VarExpr;
struct ConstExpr;
struct AssignStmt;
struct BinOpExpr;
struct UnOpExpr;
struct CallExpr;
struct IndexExpr;
struct MemberExpr;
struct FuncExpr;

struct ASTVisitor {
    virtual ~ASTVisitor(){};

    // Types
    virtual void visit(SimpleType*, void*){};
    virtual void visit(ArrayType*, void*){};
    virtual void visit(FuncType*, void*){};

    // Statements
    virtual void visit(Block*, void*){};
    virtual void visit(Decl*, void*){};
    virtual void visit(AssignStmt*, void*){};
    virtual void visit(ForStmt*, void*){};
    virtual void visit(IfStmt*, void*){};
    virtual void visit(ReturnStmt*, void*){};

    // Expressions
    virtual void visit(VarExpr*, void*){};
    virtual void visit(ConstExpr*, void*){};
    virtual void visit(BinOpExpr*, void*){};
    virtual void visit(UnOpExpr*, void*){};
    virtual void visit(CallExpr*, void*){};
    virtual void visit(IndexExpr*, void*){};
    virtual void visit(MemberExpr*, void*){};
    virtual void visit(FuncExpr*, void*){};
};

struct Node {
    virtual ~Node(){};

    // Location info for error printing
    const char* file = "[unknown]";
    int line = 0, column = 0;
    void set_location(const Token* t) {
        file = t->file;
        line = t->line;
        column = t->column;
    }
    void set_location(const Node* n) {
        file = n->file;
        line = n->line;
        column = n->column;
    }

    // Every node belongs to a parent block except the top level Block,
    // this gets assigned by analyze_blocks stage.
    Block* block = nullptr;

    virtual std::string print(const std::string& prefix = "") = 0;
    virtual void accept(ASTVisitor* visitor, void* userdata = nullptr) = 0;
};

#define visitor_accept                                                   \
    virtual void accept(ASTVisitor* visitor, void* userdata = nullptr) { \
        assert(visitor);                                                 \
        visitor->visit(this, userdata);                                  \
    }

// Types

struct Type : public Node {
    virtual ~Type(){};

    virtual std::string print(const std::string& prefix = "") = 0;

    virtual SimpleType* to_simple_type() { return nullptr; }
    virtual ArrayType* to_array_type() { return nullptr; }
    virtual FuncType* to_func_type() { return nullptr; }

    bool operator==(Type& t) {
        // Very silly equality operator - prints to string and allocs memory
        // every time!
        // TODO(mitkus): fix this
        return print() == t.print();
    }
};

struct SimpleType : public Type {
    visitor_accept;
    virtual ~SimpleType(){};

    Str name;

    SimpleType(const Str& name) : name(name) {}

    SimpleType(const SimpleType& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        return str_join({prefix, name.tostring()});
    }

    virtual SimpleType* to_simple_type() { return this; }
};

struct ArrayType : public Type {
    visitor_accept;
    virtual ~ArrayType(){};

    Type* elem;
    bool dynamic;
    int64 static_size;

    ArrayType(Type* elem, bool dynamic = true, int64 static_size = 0)
        : elem(elem), dynamic(dynamic), static_size(static_size) {}

    ArrayType(const ArrayType& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        if(dynamic)
            return str_join({prefix, elem->print(), std::string("[]")});
        else {
            char num[64] = {0};
            sprintf(num, "%llu", static_size);
            return str_join({prefix, elem->print(), "[", num, "]"});
        }
    }

    virtual ArrayType* to_array_type() { return this; }
};

struct FuncType : public Type {
    visitor_accept;
    virtual ~FuncType(){};

    std::vector<Type*> args;
    Type* result;

    FuncType(const std::vector<Type*> args, Type* result)
        : args(args), result(result) {}

    FuncType(const FuncType& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        if(args.size() == 0 && result == nullptr) return prefix + "func()";

        std::vector<std::string> args_str;
        for(Type* t : args) {
            args_str.push_back(t->print());
        }

        if(result == nullptr) {
            return str_join({prefix, "func(", str_join(args_str, ", "), ")"});
        } else {
            return str_join({prefix,
                             "func(",
                             str_join(args_str, ", "),
                             "): ",
                             result->print()});
        }
    }

    virtual FuncType* to_func_type() { return this; }
};

struct Stmt : public Node {
    virtual ~Stmt(){};
    virtual std::string print(const std::string& prefix = "") = 0;
};

// Should expression be a statement?
struct Expr : public Stmt {
    virtual ~Expr(){};
    virtual std::string print(const std::string& prefix = "") = 0;
    virtual Type* type() = 0;
};

struct Decl : public Stmt {
    visitor_accept;
    virtual ~Decl(){};

    Str name;
    Type* t;
    Expr* expr;  // can be null

    Decl(const Str& name, Type* type, Expr* expr)
        : name(name), t(type), expr(expr) {}

    Decl(const Decl& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        if(expr)
            return str_join({prefix,
                             "var ",
                             name.tostring(),
                             ": ",
                             t->print(),
                             " = ",
                             expr->print()});
        else
            return str_join(
                {prefix, "var ", name.tostring(), ": ", t->print()});
    }
};

struct Block : public Stmt {
    visitor_accept;
    virtual ~Block(){};

    std::vector<Stmt*> statements;

    Block(const std::vector<Stmt*> statements) : statements(statements) {}

    Block(const Block& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        if(statements.empty()) return str_join({prefix, "{}"});

        std::vector<std::string> parts(statements.size() + 2);

        // Open brace stays on the same line, no prefix
        parts.push_back(prefix);
        parts.push_back("{\n");
        std::string new_prefix = prefix + indent();
        for(Stmt* s : statements)
            parts.push_back(str_join({s->print(new_prefix), "\n"}));
        parts.push_back(prefix + "}");

        return str_join(parts);
    }
};

// Expressions

struct VarExpr : public Expr {
    visitor_accept;
    virtual ~VarExpr(){};

    Str name;
    Type* t;

    VarExpr(const Str& name) : name(name), t(nullptr) {}

    VarExpr(const VarExpr& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        return str_join({prefix, name.tostring()});
    }

    virtual Type* type() { return t; }
};

struct ConstExpr : public Expr {
    visitor_accept;
    virtual ~ConstExpr(){};

    Token val;
    SimpleType t;

    ConstExpr(Token val) : val(val), t("Unresolved") {
        set_location(&val);

        // Resolve type
        switch(val.type) {
            case TOK_BOOL:
                t.name = "bool";
                break;
            case TOK_INTEGER:
                t.name = "i64";
                break;
            case TOK_FLOAT:
                t.name = "f32";
                break;
            case TOK_STRING:
                t.name = "str";
                break;
            default:
                assert(0 && "Unexpected token type in ConstExpr");
        }
    }

    ConstExpr(const ConstExpr& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        switch(val.type) {
            case TOK_BOOL:
            case TOK_INTEGER:
            case TOK_FLOAT:
                return str_join({prefix, val.str.tostring()});
            case TOK_STRING: {
                std::string str;
                if(val.parse_string(str))
                    return str_join({prefix, "\"", str, "\""});
                else
                    return str_join({prefix, "[bad string token]"});
            }
            default:
                return str_join({prefix, "[wrong token type]"});
        }
    }

    virtual Type* type() { return &t; }
};

struct BinOpExpr : public Expr {
    visitor_accept;
    virtual ~BinOpExpr(){};

    Token op;
    Expr* lhs;
    Expr* rhs;
    Type* t;

    BinOpExpr(Token op, Expr* lhs, Expr* rhs)
        : op(op), lhs(lhs), rhs(rhs), t(nullptr) {
        set_location(&op);
    }

    BinOpExpr(const BinOpExpr& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        return str_join(
            {prefix, lhs->print(), " ", op.str.tostring(), " ", rhs->print()});
    }

    virtual Type* type() {
        if(t) return t;
        return rhs->type();
    }
};

struct UnOpExpr : public Expr {
    visitor_accept;
    virtual ~UnOpExpr(){};

    Token op;
    Expr* right;

    UnOpExpr(Token op, Expr* right) : op(op), right(right) {
        set_location(&op);
    }

    UnOpExpr(const UnOpExpr& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        return str_join({prefix, op.str.tostring(), right->print()});
    }

    virtual Type* type() { return right->type(); }
};

struct CallExpr : public Expr {
    visitor_accept;
    virtual ~CallExpr(){};

    Str name;
    std::vector<Expr*> args;
    Type* t;

    CallExpr(const Str& name, const std::vector<Expr*> args)
        : name(name), args(args), t(nullptr) {}

    CallExpr(const CallExpr& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        std::vector<std::string> parts;

        parts.push_back(prefix);
        parts.push_back(name.tostring());
        parts.push_back("(");
        if(!args.empty()) {
            std::vector<std::string> args_str;
            for(Expr* e : args) {
                args_str.push_back(e->print());
            }
            parts.push_back(str_join(args_str, ", "));
        }
        parts.push_back(")");

        return str_join(parts);
    }

    virtual Type* type() { return t; }
};

struct IndexExpr : public Expr {
    visitor_accept;
    virtual ~IndexExpr(){};

    Expr* array;
    Expr* index;
    Type* t;

    IndexExpr(Expr* array, Expr* index) : array(array), index(index) {}

    IndexExpr(const IndexExpr& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        return str_join({prefix, array->print(), "[", index->print(), "]"});
    }

    virtual Type* type() { return t; }
};

struct MemberExpr : public Expr {
    visitor_accept;
    virtual ~MemberExpr(){};

    Expr* structure;
    Str member;
    Type* t;

    MemberExpr(Expr* structure, const Str& member)
        : structure(structure), member(member) {}

    MemberExpr(const MemberExpr& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        return str_join({prefix, structure->print(), ".", member.tostring()});
    }

    virtual Type* type() { return t; }
};

struct FuncExpr : public Expr {
    visitor_accept;
    virtual ~FuncExpr(){};

    FuncType* t;
    Str name;
    std::vector<Str> args;
    Block* body;

    FuncExpr(FuncType* t,
             const Str& name,
             const std::vector<Str>& args,
             Block* body)
        : t(t), name(name), args(args), body(body) {}

    FuncExpr(const FuncExpr& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        assert(args.size() == t->args.size());

        std::vector<std::string> parts;

        parts.push_back(prefix);
        parts.push_back("func ");
        parts.push_back(name.tostring());
        parts.push_back("(");

        if(!args.empty()) {
            for(int i = 0; i < args.size(); ++i) {
                parts.push_back(args[i].tostring());
                parts.push_back(": ");
                parts.push_back(t->args[i]->print());
                if(i < args.size() - 1) parts.push_back(", ");
            }
        }

        parts.push_back(")");

        if(t->result) {
            parts.push_back(": ");
            parts.push_back(t->result->print());
        }

        parts.push_back(" ");
        parts.push_back(body->print(prefix));

        return str_join(parts);
    }

    virtual Type* type() { return t; }
};

// Statements

struct AssignStmt : public Stmt {
    visitor_accept;
    virtual ~AssignStmt(){};

    Token op;
    VarExpr* lhs;
    Expr* rhs;

    AssignStmt(Token op, VarExpr* lhs, Expr* rhs) : op(op), lhs(lhs), rhs(rhs) {
        set_location(&op);
    }

    AssignStmt(const AssignStmt&) = default;

    virtual std::string print(const std::string& prefix = "") {
        return str_join(
            {prefix, lhs->print(), " ", op.str.tostring(), " ", rhs->print()});
    }

    virtual Type* type() { return rhs->type(); }
};

struct ForStmt : public Stmt {
    visitor_accept;
    virtual ~ForStmt(){};

    Stmt* init;
    Expr* check;
    AssignStmt* update;
    Block* body;

    ForStmt(Stmt* init, Expr* check, AssignStmt* update, Block* body)
        : init(init), check(check), update(update), body(body) {}

    ForStmt(const ForStmt& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        std::vector<std::string> parts;

        parts.push_back(prefix);
        parts.push_back("for (");
        if(init) parts.push_back(init->print());
        parts.push_back(";");
        if(check) parts.push_back(check->print(" "));
        parts.push_back(";");
        if(update) parts.push_back(update->print(" "));
        parts.push_back(")");
        if(body) {
            parts.push_back("\n");
            parts.push_back(body->print(prefix));
        }

        return str_join(parts);
    }
};

struct IfStmt : public Stmt {
    visitor_accept;
    virtual ~IfStmt(){};

    Expr* cond;
    Stmt* iftrue;
    Stmt* iffalse;

    IfStmt(Expr* cond, Stmt* iftrue, Stmt* iffalse)
        : cond(cond), iftrue(iftrue), iffalse(iffalse) {}

    IfStmt(const IfStmt& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        std::vector<std::string> parts;

        parts.push_back(prefix);
        parts.push_back("if (");
        parts.push_back(cond->print());
        parts.push_back(")\n");
        parts.push_back(iftrue->print(indent()));
        if(iffalse) {
            parts.push_back("\n");
            parts.push_back(prefix);
            parts.push_back("else\n");
            parts.push_back(iffalse->print(indent()));
        }

        return str_join(parts);
    }
};

struct ReturnStmt : public Stmt {
    visitor_accept;
    virtual ~ReturnStmt(){};

    Expr* expr;

    ReturnStmt(Expr* expr) : expr(expr) {}

    ReturnStmt(const ReturnStmt& v) = default;

    virtual std::string print(const std::string& prefix = "") {
        if(expr)
            return str_join({prefix, "return ", expr->print()});
        else
            return str_join({prefix, "return"});
    }
};

// Helper types used in parsing. Could be in separate file?

struct ExprList {
    Expr* expr;
    ExprList* next;

    ExprList() : expr(nullptr), next(nullptr){};

    ExprList* add(Expr* e, LinAlloc* allocator) {
        if(next) {
            next->add(e, allocator);
        } else {
            assert(expr == nullptr);
            expr = e;
            next = allocator->alloc(ExprList());
        }
        return this;
    }

    std::vector<Expr*> tovector() {
        std::vector<Expr*> res;
        ExprList* e = this;
        while(e != nullptr) {
            if(e->expr) res.push_back(e->expr);
            e = e->next;
        }
        return res;
    }
};

struct ArgList {
    Str name;
    Type* type;
    ArgList* next;

    ArgList() : name(""), type(nullptr), next(nullptr){};

    ArgList* add(Str n, Type* t, LinAlloc* allocator) {
        if(next) {
            next->add(n, t, allocator);
        } else {
            assert(type == nullptr);
            name = n;
            type = t;
            next = allocator->alloc(ArgList());
        }
        return this;
    }

    std::vector<Str> arg_names() {
        std::vector<Str> res;
        ArgList* a = this;
        while(a != nullptr) {
            if(a->type) res.push_back(a->name);
            a = a->next;
        }
        return res;
    }

    std::vector<Type*> arg_types() {
        std::vector<Type*> res;
        ArgList* a = this;
        while(a != nullptr) {
            if(a->type) res.push_back(a->type);
            a = a->next;
        }
        return res;
    }
};

// Visitor types

// Default logic for walking non-type nodes
struct ASTWalkVisitor : public ASTVisitor {
    virtual ~ASTWalkVisitor(){};

    virtual bool should_continue() { return true; }

    virtual void visit(SimpleType*, void*){};
    virtual void visit(ArrayType*, void*){};
    virtual void visit(FuncType*, void*){};

    virtual void visit(VarExpr*, void*){};
    virtual void visit(ConstExpr*, void*){};

    virtual void visit(Block* b, void* parent) {
        for(Stmt* s : b->statements) {
            s->accept(this, parent);
            if(!should_continue()) return;
        }
    }

    virtual void visit(Decl* d, void* parent) {
        if(d->expr) {
            d->expr->accept(this, parent);
        }
    }

    virtual void visit(ForStmt* f, void* parent) {
        if(f->init) {
            f->init->accept(this, parent);
            if(!should_continue()) return;
        }
        if(f->check) {
            f->check->accept(this, parent);
            if(!should_continue()) return;
        }

        if(f->update) {
            f->update->accept(this, parent);
            if(!should_continue()) return;
        }
        if(f->body) {
            f->body->accept(this, parent);
            if(!should_continue()) return;
        }
    }

    virtual void visit(IfStmt* i, void* parent) {
        i->cond->accept(this, parent);
        if(!should_continue()) return;
        if(i->iftrue) {
            i->iftrue->accept(this, parent);
            if(!should_continue()) return;
        }
        if(i->iffalse) {
            i->iffalse->accept(this, parent);
            if(!should_continue()) return;
        }
    }

    virtual void visit(ReturnStmt* r, void* parent) {
        if(r->expr) r->expr->accept(this, parent);
    }

    virtual void visit(AssignStmt* a, void* parent) {
        a->lhs->accept(this, parent);
        if(!should_continue()) return;
        a->rhs->accept(this, parent);
    }

    virtual void visit(BinOpExpr* b, void* parent) {
        b->lhs->accept(this, parent);
        if(!should_continue()) return;
        b->rhs->accept(this, parent);
    }

    virtual void visit(UnOpExpr* u, void* parent) {
        u->right->accept(this, parent);
    }

    virtual void visit(CallExpr* c, void* parent) {
        for(Expr* e : c->args) {
            e->accept(this, parent);
            if(!should_continue()) return;
        }
    }

    virtual void visit(IndexExpr* i, void* parent) {
        i->array->accept(this, parent);
        if(!should_continue()) return;
        i->index->accept(this, parent);
    }

    virtual void visit(MemberExpr* m, void* parent) {
        m->structure->accept(this, parent);
    }

    virtual void visit(FuncExpr* f, void* parent) {
        f->body->accept(this, parent);
    }
};
