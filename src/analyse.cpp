#include "analyse.hpp"

Decl* BlockInfo::get_decl(Str name) {
    auto itr = decls.find(name);
    if(itr != decls.end()) return itr->second;
    if(parent) return parent->get_decl(name);
    return nullptr;
}

FuncExpr* BlockInfo::get_func(Str name) {
    auto itr = funcs.find(name);
    if(itr != funcs.end()) return itr->second;
    if(parent) return parent->get_func(name);
    return nullptr;
}

bool BlockInfo::decl_used_before_shadowing(Str name) {
    if(shadowed_decls.find(name) != shadowed_decls.end()) {
        if(decls.find(name) == decls.end()) return true;
    }
    return false;
}

bool BlockInfo::func_used_before_shadowing(Str name) {
    if(shadowed_funcs.find(name) != shadowed_funcs.end()) {
        if(funcs.find(name) == funcs.end()) return true;
    }
    return false;
}

BlockInfo* analyse_get_node_bi(BlockInfoMap& m, Node* n) {
    if(n->block) {
        auto itr = m.find(n->block);
        if(itr != m.end()) return itr->second;
    }
    return nullptr;
}

struct TopLevelBlockVisitor : public ASTVisitor {
    Block* result = nullptr;

    virtual ~TopLevelBlockVisitor(){};

    virtual void visit(Block* b, void*) { result = b; }
};

Block* analyse_get_tl_block(Node* ast) {
    TopLevelBlockVisitor v;
    ast->accept(&v);
    return v.result;
}

struct BuildShadowsMapVisitor : public ASTVisitor {
    BlockInfo* bi;

    BuildShadowsMapVisitor(BlockInfo* bi) : bi(bi) {}

    virtual void visit(Decl* d, void* parent) {
        BlockInfo* parent_bi = (BlockInfo*)parent;

        if(parent_bi && parent_bi->get_decl(d->name)) {
            bi->shadowed_decls[d->name] = d;
        }
    }

    virtual void visit(FuncExpr* f, void* parent) {
        BlockInfo* parent_bi = (BlockInfo*)parent;

        if(parent_bi && parent_bi->get_func(f->name)) {
            bi->shadowed_funcs[f->name] = f;
        }
    }
};

struct AnalyseBlocksVisitor : public ASTWalkVisitor {
    LinAlloc* allocator;
    BlockInfoMap& blmap;
    const char* file;
    Error err;

    AnalyseBlocksVisitor(LinAlloc* allocator,
                         BlockInfoMap& blmap,
                         const char* file)
        : allocator(allocator), blmap(blmap), file(file) {}

    virtual ~AnalyseBlocksVisitor(){};

    virtual bool should_continue() { return err.success(); }

    BlockInfo* visit_block(Block* b,
                           BlockInfo* parent,
                           const std::vector<Stmt*>& statements) {
        BlockInfo* bi = allocator->alloc(BlockInfo(b, parent));

        // Scan first to build shadowed decls/funcs maps
        BuildShadowsMapVisitor vis(bi);
        for(Stmt* s : statements) {
            s->accept(&vis, (void*)parent);
        }

        for(Stmt* s : statements) {
            s->accept(this, (void*)bi);
            if(!should_continue()) return bi;
        }

        blmap[b] = bi;
        return bi;
    }

    bool add_decl(BlockInfo* bi, const Str& name, Decl* d) {
        if(bi->decls.find(name) == bi->decls.end()) {
            bi->decls[name] = d;
            return true;
        }
        return false;
    }

    bool add_func(BlockInfo* bi, const Str& name, FuncExpr* f) {
        if(bi->funcs.find(name) == bi->funcs.end()) {
            bi->funcs[name] = f;
            return true;
        }
        return false;
    }

    // ASTVisitor interface

    // Types, just assign block
    virtual void visit(SimpleType* t, void* bi) {
        BlockInfo* block_bi = (BlockInfo*)bi;
        t->block = block_bi->ast_block;
    }

    virtual void visit(ArrayType* t, void* bi) {
        BlockInfo* block_bi = (BlockInfo*)bi;
        t->block = block_bi->ast_block;
        t->elem->accept(this, bi);
    }

    virtual void visit(FuncType* t, void* bi) {
        BlockInfo* block_bi = (BlockInfo*)bi;
        t->block = block_bi->ast_block;
        for(Type* arg : t->args) {
            arg->accept(this, bi);
            if(!should_continue()) return;
        }
        if(t->result) t->result->accept(this, bi);
    }

    // Construct parenting structure and fill func/decl maps
    // for Block, FuncExpr, Decl

    virtual void visit(Block* b, void* parent) {
        BlockInfo* block_bi = (BlockInfo*)parent;
        if(parent)
            b->block = block_bi->ast_block;
        else
            b->block = nullptr;
        visit_block(b, (BlockInfo*)parent, b->statements);
    }

    virtual void visit(FuncExpr* f, void* parent) {
        assert(parent);

        Block* b = f->body;
        BlockInfo* parent_bi = (BlockInfo*)parent;
        BlockInfo* bi = allocator->alloc(BlockInfo(b, parent_bi));

        blmap[b] = bi;
        f->block = parent_bi->ast_block;

        f->t->accept(this, parent);
        if(!should_continue()) return;

        bi->func = f;
        if(!add_func(parent_bi, f->name, f)) {
            FuncExpr* prev = parent_bi->funcs[f->name];
            err.set_msg(allocator,
                        "func %s already declared\n Previous declaration in %s "
                        "line %d.",
                        f->name.tostring().c_str(),
                        prev->file,
                        prev->line);
            err.set(ERR_ANALYSE_FUNC_ALREADY_DECLARED, f);
            return;
        }

        for(int i = 0; i < f->args.size(); ++i) {
            Type* arg_type = f->t->args[i];
            Str arg_name = f->args[i];
            Decl* arg_decl =
                allocator->alloc(Decl(arg_name, arg_type, nullptr));
            arg_decl->set_location(f);
            bi->decls[arg_name] = arg_decl;
        }

        // Scan first to build shadowed decls/funcs maps
        BuildShadowsMapVisitor vis(bi);
        for(Stmt* s : f->body->statements) {
            s->accept(&vis, parent);
        }

        for(Stmt* s : f->body->statements) {
            s->accept(this, (void*)bi);
            if(!should_continue()) return;
        }
    }

    virtual void visit(Decl* d, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        d->block = bi->ast_block;
        d->t->accept(this, parent);
        if(!should_continue()) return;
        if(d->expr) {
            d->expr->accept(this, parent);
            if(!should_continue()) return;
        }

        if(!add_decl(bi, d->name, d)) {
            Decl* prev = bi->decls[d->name];
            assert(prev->name == d->name);
            err.set_msg(
                allocator,
                "var %s already declared\n Previous declaration in %s line %d.",
                d->name.ptr,
                prev->file,
                prev->line);
            err.set(ERR_ANALYSE_VAR_ALREADY_DECLARED, d);
            return;
        }
    }

    // Propagate block parents for all other nodes

    virtual void visit(AssignStmt* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        n->lhs->accept(this, parent);
        if(!should_continue()) return;
        n->rhs->accept(this, parent);
    }

    virtual void visit(ForStmt* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        if(n->init) {
            n->init->accept(this, parent);
            if(!should_continue()) return;
        }
        if(n->check) {
            n->check->accept(this, parent);
            if(!should_continue()) return;
        }
        if(n->update) {
            n->update->accept(this, parent);
            if(!should_continue()) return;
        }

        n->body->accept(this, parent);
    }

    virtual void visit(IfStmt* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        n->cond->accept(this, parent);
        if(!should_continue()) return;
        if(n->iftrue) {
            n->iftrue->accept(this, parent);
            if(!should_continue()) return;
        }
        if(n->iffalse) {
            n->iffalse->accept(this, parent);
            if(!should_continue()) return;
        }
    }

    virtual void visit(ReturnStmt* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        if(n->expr) n->expr->accept(this, parent);
    }

    virtual void visit(VarExpr* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        if(bi->get_decl(n->name) == nullptr) {
            err.set_msg(
                allocator, "undefined variable %s", n->name.tostring().c_str());
            err.set(ERR_ANALYSE_USE_UNDECLARED_VARIABLE, n);
            return;
        }

        if(bi->decl_used_before_shadowing(n->name)) {
            Decl* shadow = bi->shadowed_decls[n->name];
            err.set_msg(allocator,
                        "decl %s is used before shadowing\n"
                        " Will be shadowed in %s line %d",
                        n->name.tostring().c_str(),
                        shadow->file,
                        shadow->line);
            err.set(ERR_ANALYSE_USE_BEFORE_SHADOWING, n);
            return;
        }
    }

    virtual void visit(ConstExpr* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        n->t.accept(this, parent);
    }

    virtual void visit(BinOpExpr* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        n->lhs->accept(this, parent);
        if(!should_continue()) return;
        n->rhs->accept(this, parent);
    }

    virtual void visit(UnOpExpr* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        n->right->accept(this, parent);
    }

    virtual void visit(CallExpr* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        if(bi->func_used_before_shadowing(n->name)) {
            FuncExpr* shadow = bi->shadowed_funcs[n->name];
            err.set_msg(allocator,
                        "func %s is called before shadowing\n"
                        " Will be shadowed in %s line %d",
                        n->name.tostring().c_str(),
                        shadow->file,
                        shadow->line);
            err.set(ERR_ANALYSE_CALL_BEFORE_SHADOWING, n);
            return;
        }

        for(Expr* e : n->args) {
            e->accept(this, parent);
            if(!should_continue()) return;
        }
    }

    virtual void visit(IndexExpr* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        n->array->accept(this, parent);
        if(!should_continue()) return;
        n->index->accept(this, parent);
    }

    virtual void visit(MemberExpr* n, void* parent) {
        assert(parent);
        BlockInfo* bi = (BlockInfo*)parent;
        n->block = bi->ast_block;

        n->structure->accept(this, parent);
    }
};

Error analyse_blocks(Node* ast,
                     LinAlloc* allocator,
                     BlockInfoMap& blmap,
                     const char* file) {
    Block* b = analyse_get_tl_block(ast);
    assert(b);

    AnalyseBlocksVisitor v(allocator, blmap, file);
    ast->accept(&v);
    return v.err;
}

// Expand this to a full type tracking system
const char* builtin_types[] = {"i8",
                               "u8",
                               "i16",
                               "u16",
                               "i32",
                               "u32",
                               "i64",
                               "u64",
                               "f32",
                               "f64",
                               "bool",
                               "str"};

static bool is_integer_type(Str type) {
    return type == "i64" || type == "u64" || type == "i32" || type == "u32" ||
           type == "i16" || type == "u16" || type == "i8" || type == "u8";
}

struct AnalyseTypesVisitor : public ASTWalkVisitor {
    LinAlloc* allocator;
    BlockInfoMap& blmap;
    const char* file;
    Error err;

    AnalyseTypesVisitor(LinAlloc* allocator,
                        BlockInfoMap& blmap,
                        const char* file)
        : allocator(allocator), blmap(blmap), file(file) {}

    virtual ~AnalyseTypesVisitor(){};

    virtual bool should_continue() { return err.success(); }

    virtual void visit(AssignStmt* a, void* parent) {
        a->lhs->accept(this, parent);
        if(!should_continue()) return;
        a->rhs->accept(this, parent);
        if(!should_continue()) return;

        Type* lhs_type = a->lhs->type();
        Type* rhs_type = a->rhs->type();

        if(!(*lhs_type == *rhs_type)) {
            err.set_msg(allocator,
                        "assigning expression of type %s to variable %s which "
                        "has incompatible type %s",
                        rhs_type->print().c_str(),
                        a->lhs->name.tostring().c_str(),
                        lhs_type->print().c_str());
            err.set(ERR_ANALYSE_TYPE_MISMATCH, a);
            return;
        }
    }

    virtual void visit(BinOpExpr* e, void* parent) {
        e->lhs->accept(this, parent);
        if(!should_continue()) return;
        e->rhs->accept(this, parent);
        if(!should_continue()) return;

        // Comparisons have bool type
        switch(e->op.type) {
            case TOK_OP_LESS:
            case TOK_OP_LESSEQ:
            case TOK_OP_MORE:
            case TOK_OP_MOREEQ:
            case TOK_OP_EQ:
            case TOK_OP_NOTEQ:
                e->t = allocator->alloc(SimpleType("bool"));
                e->t->set_location(e);
            default:
                break;
        }

        assert(e->lhs->type());
        assert(e->rhs->type());
        if(!(*e->lhs->type() == *e->rhs->type())) {
            err.set_msg(allocator,
                        "binary expression %s has conflicting types\n lhs is "
                        "%s, rhs is %s",
                        e->print().c_str(),
                        e->lhs->type()->print().c_str(),
                        e->rhs->type()->print().c_str());
            err.set(ERR_ANALYSE_TYPE_MISMATCH, e);
            return;
        }
    }

    virtual void visit(VarExpr* v, void* parent) {
        if(v->t == nullptr) {
            BlockInfo* bi = analyse_get_node_bi(blmap, v);
            assert(bi);
            Decl* decl = bi->get_decl(v->name);
            // Use of undeclared variable is checked in blocks analyser
            assert(decl != nullptr);
            v->t = decl->t;
        }
    }

    virtual void visit(IndexExpr* i, void* parent) {
        i->array->accept(this, parent);
        if(!should_continue()) return;
        i->index->accept(this, parent);
        if(!should_continue()) return;

        if(i->t == nullptr) {
            ArrayType* array_type = i->array->type()->to_array_type();
            if(!array_type) {
                err.set_msg(allocator,
                            "array in expression %s is expected to have array "
                            "type, instead the type is %s",
                            i->print().c_str(),
                            i->array->type()->print().c_str());
                err.set(ERR_ANALYSE_TYPE_MISMATCH, i);
                return;
            }

            SimpleType* index_type = i->index->type()->to_simple_type();
            if(!index_type || !is_integer_type(index_type->name)) {
                err.set_msg(allocator,
                            "index in expression %s is expected to have simple "
                            "integer type, instead the type is %s",
                            i->print().c_str(),
                            i->index->type()->print().c_str());
                err.set(ERR_ANALYSE_TYPE_MISMATCH, i);
                return;
            }

            i->t = array_type->elem;
        }
    }

    virtual void visit(CallExpr* c, void* parent) {
        for(Expr* arg : c->args) {
            arg->accept(this, parent);
            if(!should_continue()) return;
        }

        if(c->t == nullptr) {
            BlockInfo* bi = analyse_get_node_bi(blmap, c);
            assert(bi);
            FuncExpr* func = bi->get_func(c->name);
            if(!func) {
                err.set_msg(allocator,
                            "use of undeclared function %s",
                            c->name.tostring().c_str());
                err.set(ERR_ANALYSE_USE_UNDECLARED_FUNCTION, c);
                return;
            }

            if(c->args.size() != func->args.size()) {
                err.set_msg(allocator,
                            "function %s is expected to take %d arguments but "
                            "%d was given",
                            func->name.tostring().c_str(),
                            (int)func->args.size(),
                            (int)c->args.size());
                err.set(ERR_ANALYSE_WRONG_NUMBER_OF_ARGUMENTS, c);
                return;
            }

            for(int i = 0; i < c->args.size(); ++i) {
                Type* arg_type = c->args[i]->type();
                assert(arg_type);
                Type* expected_type = func->t->args[i];
                assert(expected_type);

                if(!(*arg_type == *expected_type)) {
                    err.set_msg(allocator,
                                "argument %d to function %s was expected to be "
                                "of type %s, but is %s instead",
                                i,
                                func->name.tostring().c_str(),
                                expected_type->print().c_str(),
                                arg_type->print().c_str());
                    err.set(ERR_ANALYSE_TYPE_MISMATCH, c);
                    return;
                }
            }

            c->t = func->t->result;
        }
    }

    virtual void visit(Decl* d, void* parent) {
        if(d->expr) {
            d->expr->accept(this, parent);
            if(!should_continue()) return;

            Type* decl_type = d->t;
            Type* expr_type = d->expr->type();

            if(!(*decl_type == *expr_type)) {
                err.set_msg(
                    allocator,
                    "variable declaration for %s is expected to have type %s, "
                    "but initialisation expression has type %s",
                    d->name.tostring().c_str(),
                    decl_type->print().c_str(),
                    expr_type->print().c_str());
                err.set(ERR_ANALYSE_TYPE_MISMATCH, d);
                return;
            }
        }
    }
};

Error analyse_types(Node* ast,
                    LinAlloc* allocator,
                    BlockInfoMap& blmap,
                    const char* file) {
    Block* b = analyse_get_tl_block(ast);
    assert(b);

    AnalyseTypesVisitor v(allocator, blmap, file);
    ast->accept(&v);
    return v.err;
}
