#pragma once

#include <string>
#include <vector>

std::string str_join(const std::vector<std::string>& elems);
std::string str_join(const std::vector<std::string>& elems, const char* delim);
