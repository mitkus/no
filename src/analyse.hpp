#pragma once

#include "ast.hpp"
#include "error.hpp"

#include <unordered_map>

// Program analysis data types

struct BlockInfo {
    Block* ast_block;
    BlockInfo* parent;
    FuncExpr* func;  // Non-null in FuncExpr blocks

    std::unordered_map<Str, Decl*, StrHash> decls;
    std::unordered_map<Str, FuncExpr*, StrHash> funcs;

    // These get built before AnalyseBlocks main step,
    // used to check for use-before-shadowing bugs.
    std::unordered_map<Str, Decl*, StrHash> shadowed_decls;
    std::unordered_map<Str, FuncExpr*, StrHash> shadowed_funcs;

    BlockInfo(Block* ast_block, BlockInfo* parent, FuncExpr* func = nullptr)
        : ast_block(ast_block), parent(parent), func(func) {}

    BlockInfo(const BlockInfo& v) = default;

    Decl* get_decl(Str name);
    FuncExpr* get_func(Str name);

    bool decl_used_before_shadowing(Str name);
    bool func_used_before_shadowing(Str name);
};

typedef std::unordered_map<Node*, BlockInfo*> BlockInfoMap;

BlockInfo* analyse_get_node_bi(BlockInfoMap& m, Node* n);

Block* analyse_get_tl_block(Node* ast);

// Fills out BlockInfoMap, every Block gets a corresponding BlockInfo struct
Error analyse_blocks(Node* ast,
                     LinAlloc* allocator,
                     BlockInfoMap& blmap,
                     const char* file = "");

// Infers all undefined types in the AST, checks type compatibility
Error analyse_types(Node* ast,
                    LinAlloc* allocator,
                    BlockInfoMap& blmap,
                    const char* file = "");
