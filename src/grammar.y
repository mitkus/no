%include {
#include <stdio.h>
#include "parser.hpp"

#define alloc_ast(node) pparams->allocator->alloc(node)
}

%token_type { const Token* }
%token_prefix PTOK_
%extra_argument { ParserParams* pparams }

%syntax_error {
    pparams->error.file = pparams->file;
    if(yymajor == 0) {
        pparams->error.msg = "unexpected end of input";
        pparams->error.type = ERR_PARSE_UNEXPECTED_END;
    }
    else {
        char* msg = pparams->allocator->alloc_str(256);
        snprintf(msg, 256, "unexpected token \"%s\"", TOKEN->str.tostring().c_str());
        pparams->error.msg = msg;
        pparams->error.line = TOKEN->line;
        pparams->error.column = TOKEN->column;
        pparams->error.file = TOKEN->file;
        pparams->error.type = ERR_PARSE_UNEXPECTED_TOKEN;
    }
}

%parse_failure { pparams->out = nullptr; }

%destructor main { (void)pparams; }
%type main { Node* }
main ::= block_seq(A). { pparams->out = alloc_ast(Block(A->statements)); }

%type block { Block* }
%type block_seq { Block* }
block(R) ::= SYN_OPEN_BRACE(L) block_seq(A) SYN_CLOSE_BRACE. {
    R = A;
    R->set_location(L);
}
block_seq(R) ::= block_seq(A) statement(B). { A->statements.push_back(B); R = A; }
block_seq(R) ::= . { R = alloc_ast(Block({})); }

%type statement { Stmt* }
statement(R) ::= expr(A) SYN_SEMICOLON. { R = A; }
statement(R) ::= decl(A) SYN_SEMICOLON. { R = A; }
statement(R) ::= return_stmt(A) SYN_SEMICOLON. { R = A; }
statement(R) ::= assign_stmt(A) SYN_SEMICOLON. { R = A; }
statement(R) ::= block(A). { R = A; }
statement(R) ::= func_expr(A). { R = A; }
statement(R) ::= if_stmt(A). { R = A; }
statement(R) ::= for_stmt(A). { R = A; }

%type expr { Expr* }
expr(R) ::= SYN_OPEN_PAREN expr(A) SYN_CLOSE_PAREN. { R = A; }
expr(R) ::= var_expr(A). { R = A; }
expr(R) ::= const_expr(A). { R = A; }
expr(R) ::= call_expr(A). { R = A; }
expr(R) ::= bin_expr(A). { R = A; }
expr(R) ::= func_expr(A). { R = A; }
expr(R) ::= index_expr(A). { R = A; }
expr(R) ::= member_expr(A). { R = A; }

%type maybe_empty_expr { Expr* }
maybe_empty_expr(R) ::= expr(A). { R = A; }
maybe_empty_expr(R) ::= . { R = nullptr; }

%type maybe_empty_assign { AssignStmt* }
maybe_empty_assign(R) ::= assign_stmt(A). { R = A; }
maybe_empty_assign(R) ::= . { R = nullptr; }

%type var_expr { VarExpr* }
var_expr(R) ::= IDENTIFIER(A). {
    R = alloc_ast(VarExpr(A->str));
    R->set_location(A);
}

// ConstExpr has implicit set_location
%type const_expr { ConstExpr* }
const_expr(R) ::= BOOL(A). { R = alloc_ast(ConstExpr(*A)); }
const_expr(R) ::= INTEGER(A). { R = alloc_ast(ConstExpr(*A)); }
const_expr(R) ::= FLOAT(A). { R = alloc_ast(ConstExpr(*A)); }
const_expr(R) ::= STRING(A). { R = alloc_ast(ConstExpr(*A)); }

%type call_expr { CallExpr* }
call_expr(R) ::= IDENTIFIER(A) SYN_OPEN_PAREN(L) expr_list(B) SYN_CLOSE_PAREN. {
    R = alloc_ast(CallExpr(A->str, B->tovector()));
    R->set_location(L);
}

%type index_expr { IndexExpr* }
index_expr(R) ::= expr(A) SYN_OPEN_BRACKET(L) expr(B) SYN_CLOSE_BRACKET. {
    R = alloc_ast(IndexExpr(A, B));
    R->set_location(L);
}

%type member_expr { MemberExpr* }
member_expr(R) ::= expr(A) SYN_DOT IDENTIFIER(B). {
    R = alloc_ast(MemberExpr(A, B->str));
    R->set_location(B);
}

%type expr_list { ExprList* }
%type expr_nelist { ExprList* }
expr_list(R) ::= . { R = alloc_ast(ExprList()); }
expr_list(R) ::= expr_nelist(A). { R = A; }
expr_nelist(R) ::= expr(A). { R = alloc_ast(ExprList()); R->add(A, pparams->allocator); }
expr_nelist(R) ::= expr_nelist(A) SYN_COMMA expr(B). { R = A->add(B, pparams->allocator); }

//%left OP_ASSIGN OP_PLUSASSIGN OP_MINUSASSIGN OP_MULASSIGN OP_DIVASSIGN.
%left OP_EQ OP_NOTEQ OP_MORE OP_MOREEQ OP_LESS OP_LESSEQ.
%left OP_PLUS OP_MINUS.
%left OP_MUL OP_DIV.
%nonassoc SYN_OPEN_BRACKET SYN_CLOSE_BRACKET.
//%right EXP NOT.
%left SYN_DOT.

// BinOp & AssignStmt have implicit set_location
%type bin_expr { BinOpExpr* }
bin_expr(R) ::= expr(A) OP_EQ(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }
bin_expr(R) ::= expr(A) OP_NOTEQ(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }
bin_expr(R) ::= expr(A) OP_LESS(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }
bin_expr(R) ::= expr(A) OP_LESSEQ(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }
bin_expr(R) ::= expr(A) OP_MORE(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }
bin_expr(R) ::= expr(A) OP_MOREEQ(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }

bin_expr(R) ::= expr(A) OP_PLUS(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }
bin_expr(R) ::= expr(A) OP_MINUS(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }
bin_expr(R) ::= expr(A) OP_MUL(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }
bin_expr(R) ::= expr(A) OP_DIV(O) expr(B). { R = alloc_ast(BinOpExpr(*O, A, B)); }

%type assign_stmt { AssignStmt* }
assign_stmt(R) ::= var_expr(A) OP_ASSIGN(O) expr(B). { R = alloc_ast(AssignStmt(*O, A, B)); }
assign_stmt(R) ::= var_expr(A) OP_PLUSASSIGN(O) expr(B). { R = alloc_ast(AssignStmt(*O, A, B)); }
assign_stmt(R) ::= var_expr(A) OP_MINUSASSIGN(O) expr(B). { R = alloc_ast(AssignStmt(*O, A, B)); }
assign_stmt(R) ::= var_expr(A) OP_MULASSIGN(O) expr(B). { R = alloc_ast(AssignStmt(*O, A, B)); }
assign_stmt(R) ::= var_expr(A) OP_DIVASSIGN(O) expr(B). { R = alloc_ast(AssignStmt(*O, A, B)); }

%type type { Type* }
type(R) ::= IDENTIFIER(A). {
    R = alloc_ast(SimpleType(A->str));
    R->set_location(A);
}
type(R) ::= type(A) SYN_OPEN_BRACKET SYN_CLOSE_BRACKET. {
    R = alloc_ast(ArrayType(A, true));
    R->set_location(A);
}
type(R) ::= type(A) SYN_OPEN_BRACKET INTEGER(B) SYN_CLOSE_BRACKET. {
    int64 n = 0;
    B->parse_int(n);
    R = alloc_ast(ArrayType(A, false, n));
    R->set_location(A);
}

%type decl { Decl* }
decl(R) ::= KEY_VAR IDENTIFIER(A) SYN_COLON type(B). {
    R = alloc_ast(Decl(A->str, B, nullptr));
    R->set_location(A);
}
decl(R) ::= KEY_VAR IDENTIFIER(A) SYN_COLON type(B) OP_ASSIGN expr(C). {
    R = alloc_ast(Decl(A->str, B, C));
    R->set_location(A);
}

%type maybe_empty_decl { Decl* }
maybe_empty_decl(R) ::= decl(A). { R = A; }
maybe_empty_decl(R) ::= . { R = nullptr; }

%type func_expr { FuncExpr* }
func_expr(R) ::= KEY_FUNC IDENTIFIER(A) SYN_OPEN_PAREN arg_list(B) SYN_CLOSE_PAREN block(C). {
    FuncType* t = alloc_ast(FuncType(B->arg_types(), nullptr));
    R = alloc_ast(FuncExpr(t, A->str, B->arg_names(), C));
    R->set_location(A);
}
func_expr(R) ::= KEY_FUNC IDENTIFIER(A) SYN_OPEN_PAREN arg_list(B) SYN_CLOSE_PAREN SYN_COLON type(C) block(D). {
    FuncType* t = alloc_ast(FuncType(B->arg_types(), C));
    R = alloc_ast(FuncExpr(t, A->str, B->arg_names(), D));
    R->set_location(A);
}

%type arg_list { ArgList* }
%type arg_nelist { ArgList* }
arg_list(R) ::= . { R = alloc_ast(ArgList()); }
arg_list(R) ::= arg_nelist(A). { R = A; }
arg_nelist(R) ::= IDENTIFIER(A) SYN_COLON type(B). {
    R = alloc_ast(ArgList());
    R->add(A->str, B, pparams->allocator);
}
arg_nelist(R) ::= arg_nelist(A) SYN_COMMA IDENTIFIER(B) SYN_COLON type(C). {
    R = A->add(B->str, C, pparams->allocator);
}

%type return_stmt { ReturnStmt* }
return_stmt(R) ::= KEY_RETURN(L). {
    R = alloc_ast(ReturnStmt(nullptr));
    R->set_location(L);
}
return_stmt(R) ::= KEY_RETURN(L) expr(A). {
    R = alloc_ast(ReturnStmt(A));
    R->set_location(L);
}

%type if_stmt { IfStmt* }
if_stmt(R) ::= KEY_IF(L) SYN_OPEN_PAREN expr(A) SYN_CLOSE_PAREN block(B). {
    R = alloc_ast(IfStmt(A, B, nullptr));
    R->set_location(L);
}
if_stmt(R) ::= KEY_IF(L) SYN_OPEN_PAREN expr(A) SYN_CLOSE_PAREN block(B) KEY_ELSE block(C). {
    R = alloc_ast(IfStmt(A, B, C));
    R->set_location(L);
}

%type for_stmt { ForStmt* }
for_stmt(R) ::= KEY_FOR(L) SYN_OPEN_PAREN maybe_empty_decl(A) SYN_SEMICOLON maybe_empty_expr(B) SYN_SEMICOLON maybe_empty_assign(C) SYN_CLOSE_PAREN block(D). {
    R = alloc_ast(ForStmt(A, B, C, D));
    R->set_location(L);
}

