#include "strutil.hpp"

#include <assert.h>
#include <string.h>

std::string str_join(const std::vector<std::string>& elems) {
    switch(elems.size()) {
        case 0:
            return "";
        case 1:
            return elems[0];
        default:
            size_t elems_len = 0;
            for(const std::string& s : elems) elems_len += s.size();

            size_t cur = 0;
            std::string out(elems_len, '-');
            for(const std::string& s : elems) {
                assert(cur < elems_len);
                for(char c : s) out[cur++] = c;
            }
            return out;
    }
}

std::string str_join(const std::vector<std::string>& elems, const char* delim) {
    switch(elems.size()) {
        case 0:
            return "";
        case 1:
            return elems[0];
        default:
            assert(delim);

            size_t delim_len = strlen(delim);
            size_t elems_len = 0;
            for(const std::string& s : elems) elems_len += s.size();
            size_t out_size = elems_len + delim_len * (elems.size() - 1);

            size_t cur = 0;
            std::string out(out_size, '-');
            for(size_t i = 0; i < elems.size(); ++i) {
                assert(cur < out_size);
                for(char c : elems[i]) out[cur++] = c;
                if(i < elems.size() - 1)
                    for(size_t j = 0; j < delim_len; ++j) out[cur++] = delim[j];
            }
            return out;
    }
}
