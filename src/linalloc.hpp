#pragma once

#include <assert.h>
#include "common.hpp"

// Linear memory allocator
class LinAlloc {
public:
    LinAlloc() { first = last = nullptr; }

    ~LinAlloc() {
        Chunk* c = first;
        while(c != nullptr) {
            delete[] c->data;
            Chunk* cc = c;
            c = c->next;
            delete cc;
        }
    }

    template <typename T>
    T* alloc(const T& val) {
        assert(sizeof(T) <= data_size);
        Chunk* last = get_chunk(sizeof(T));
        void* ptr = (void*)last->cur;
        last->cur += sizeof(T);
        return new(ptr) T(val);
    }

    char* alloc_str(size_t len) {
        assert(len <= data_size);
        Chunk* last = get_chunk(len);
        void* ptr = (void*)last->cur;
        last->cur += len;
        return (char*)ptr;
    }

private:
    const size_t data_size = 32 * 1024;

    struct Chunk {
        byte* data;
        byte* cur;
        Chunk* next;
    };

    Chunk* first;
    Chunk* last;

    size_t left(Chunk* c) {
        size_t used = (size_t)(c->cur - c->data);
        assert(used <= data_size);
        return data_size - used;
    }

    Chunk* new_chunk() {
        Chunk* c = new Chunk;
        c->data = c->cur = new byte[data_size];
        c->next = nullptr;
        return c;
    }

    Chunk* get_chunk(size_t need) {
        if(last == nullptr) last = first = new_chunk();

        if(left(last) < need) {
            Chunk* c = new_chunk();
            last->next = c;
            last = c;
        }

        assert(left(last) >= need);
        return last;
    }
};
