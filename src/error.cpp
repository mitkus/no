#include "error.hpp"
#include "ast.hpp"

#include <stdarg.h>
#include <stdio.h>

Error::Error(
    ErrorType type, int line, int column, const char* file, const char* msg)
    : type(type), line(line), column(column), file(file), msg(msg) {}

void Error::set_msg(LinAlloc* allocator, const char* fmt, ...) {
    const int maxlen = 384;
    va_list args;
    va_start(args, fmt);
    char* m = allocator->alloc_str(maxlen);
    vsnprintf(m, maxlen, fmt, args);
    msg = m;
    va_end(args);
}

void Error::set(ErrorType t, int l, int c, const char* f, const char* m) {
    if(m) msg = m;
    line = l;
    column = c;
    file = f;
    type = t;
}

void Error::set(ErrorType t, const Node* n, const char* m) {
    if(m) msg = m;
    file = n->file;
    line = n->line;
    column = n->column;
    type = t;
}

bool Error::success() const { return type == ERR_SUCCESS; }
