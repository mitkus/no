#include <stdio.h>
#include <string>

#include "analyse.hpp"
#include "cmdline.hpp"
#include "lex.hpp"
#include "linalloc.hpp"
#include "parser.hpp"

size_t file_size(FILE* f) {
    size_t pos = ftell(f);
    fseek(f, 0, SEEK_END);
    size_t size = ftell(f);
    fseek(f, pos, SEEK_SET);
    return size;
}

std::string readfile(const char* path) {
    FILE* f = fopen(path, "r");
    if(!f) {
        printf("Unable to read file %s\n", path);
        return std::string("");
    }

    std::string out;
    out.resize(file_size(f));

    fread(&out[0], 1, out.size(), f);

    fclose(f);

    return out;
}

cmdline::parser cmdparse() {
    cmdline::parser res;

    res.set_program_name("no");
    res.add<std::string>("output", 'o', "output file", false, "no.out");
    res.footer("filename");

    return res;
}

#ifndef TEST
int main(int argc, char* argv[]) {
    auto cmd = cmdparse();
    cmd.parse_check(argc, argv);

    std::string input;
    if(cmd.rest().size() < 1) {
        printf("%s: No input file\n", cmd.program_name().c_str());
        return -1;
    } else {
        input = cmd.rest()[0];
    }

    printf("input = %s\n", input.c_str());
    printf("output = %s\n", cmd.get<std::string>("output").c_str());

    auto source = readfile(input.c_str());

    std::vector<Token> tokens_out;
    auto err = lex_scan(source.c_str(), input.c_str(), tokens_out);
    if(err.success()) {
        LinAlloc alloc;
        Node* ast_out;
        auto err = parser_parse(tokens_out, &ast_out, &alloc, input.c_str());
        if(err.success()) {
            BlockInfoMap blmap;
            auto err = analyse_blocks(ast_out, &alloc, blmap, input.c_str());
            if(err.success()) {
                printf("Success!\n");
            } else {
                printf("Analyse error at %s line %d: %s\n",
                       err.file,
                       err.line,
                       err.msg);
                return 1;
            }

            err = analyse_types(ast_out, &alloc, blmap, input.c_str());
            if(!err.success()) {
                printf("Typecheck error at %s line %d: %s\n",
                       err.file,
                       err.line,
                       err.msg);
                return 1;
            }

        } else {
            printf(
                "Parse error at %s line %d: %s\n", err.file, err.line, err.msg);
            return 1;
        }
    } else {
        printf("Lex error at %s line %d: %s\n", err.file, err.line, err.msg);
        return 1;
    }

    return 0;
}
#endif
