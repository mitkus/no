#pragma once

#include "linalloc.hpp"

struct Node;

enum ErrorType {
    ERR_SUCCESS,
    ERR_FAILURE,

    ERR_LEX_UNEXPECTED_INPUT,
    ERR_LEX_UNCLOSED_COMMENT,
    ERR_LEX_UNCLOSED_STRING,

    ERR_PARSE_UNEXPECTED_TOKEN,
    ERR_PARSE_UNEXPECTED_END,

    ERR_ANALYSE_FUNC_ALREADY_DECLARED,
    ERR_ANALYSE_VAR_ALREADY_DECLARED,
    ERR_ANALYSE_TYPE_MISMATCH,
    ERR_ANALYSE_USE_UNDECLARED_VARIABLE,
    ERR_ANALYSE_USE_BEFORE_SHADOWING,
    ERR_ANALYSE_CALL_BEFORE_SHADOWING,
    ERR_ANALYSE_USE_UNDECLARED_FUNCTION,
    ERR_ANALYSE_WRONG_NUMBER_OF_ARGUMENTS
};

struct Error {
    ErrorType type;
    int line, column;
    const char* file;
    const char* msg;

    Error(ErrorType type = ERR_SUCCESS,
          int line = 0,
          int column = 0,
          const char* file = nullptr,
          const char* msg = nullptr);

    void set_msg(LinAlloc* allocator, const char* fmt, ...);

    void set(ErrorType t,
             int line,
             int column,
             const char* file,
             const char* msg = nullptr);

    void set(ErrorType t, const Node* n, const char* msg = nullptr);

    bool success() const;
};
