#pragma once

#include "vm_opcodes.h"

struct VmStack {
    VmStack(size_t initial_space = 1024);

    size_t size();

    T pop<T>();
    void push<T>(const T& val);

    T get<T>(size_t pos);
    void set<T>(size_t pos, const T& val);
private:
    uint32* stack;
    // TODO: add type tags
    size_t reserved;
    size_t used;
};

struct VmState {
    VmInstr* code;
    size_t code_size;

    VmStack stack;

    byte* heap;
    size_t heap_size;

    size_t pc;

    // TODO: native function table
};