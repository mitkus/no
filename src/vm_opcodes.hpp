#pragma once

#include "common.hpp"

enum VmOpcode {
    OpNull = 0,
    OpPush, // Push 32 bit immediate value on the stack
    OpLoad, // Load 32 bit value from the heap on the stack
    OpStore, // Store 32 bit value from the stack to heap
    
    OpAdd,
    OpSub,
    OpMul,
    OpDiv,

    OpJmp,
    OpCmp,
    OpJmpIf,
    OpJmpIfNot,

    OpCall,
    OpRet,

    OpCallNative,
};

struct VmInstr {
    uint32 arg1;
    uint8 op;
    uint8 arg2;
    uint8 arg3;
    uint8 arg4;
};