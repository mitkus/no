#pragma once

#include <assert.h>
#include <string.h>

// String view
struct Str {
    Str() : ptr(nullptr), len(0){};
    Str(const Str& s) : ptr(s.ptr), len(s.len){};
    Str(Str&& s) : ptr(s.ptr), len(s.len){};
    Str(const char* ptr) : ptr(ptr), len(strlen(ptr)) {}
    Str(const char* ptr, size_t len) : ptr(ptr), len(len) {}
    Str(const char* start, const char* end) {
        ptr = start;
        len = end - start;
    }
    ~Str(){};

    Str& operator=(const Str& rhs) {
        ptr = rhs.ptr;
        len = rhs.len;
        return *this;
    }

    bool operator==(const Str& rhs) const {
        if(len != rhs.len) return false;
        for(size_t i = 0; i < len; ++i) {
            if(ptr[i] != rhs.ptr[i]) return false;
        }
        return true;
    }

    bool operator==(const char* rhs) const {
        assert(rhs != nullptr);
        size_t len_rhs = strlen(rhs);
        if(len_rhs != len) return false;
        for(size_t i = 0; i < len; ++i) {
            if(ptr[i] != rhs[i]) return false;
        }
        return true;
    }

    bool operator==(const std::string& rhs) const {
        if(rhs.size() != len) return false;
        for(size_t i = 0; i < len; ++i) {
            if(ptr[i] != rhs[i]) return false;
        }
        return true;
    }

    bool startswith(const char* prefix) const {
        // TODO: Compress common code with == ?
        assert(prefix != nullptr);
        size_t len_prefix = strlen(prefix);
        if(len_prefix > len) return false;
        for(size_t i = 0; i < len_prefix; ++i) {
            if(ptr[i] != prefix[i]) return false;
        }
        return true;
    }

    bool startswith(const std::string& prefix) const {
        // TODO: Compress common code with == ?
        size_t len_prefix = prefix.size();
        if(len_prefix > len) return false;
        for(size_t i = 0; i < len_prefix; ++i) {
            if(ptr[i] != prefix[i]) return false;
        }
        return true;
    }

    std::string tostring() const {
        if(ptr != nullptr && len > 0)
            return std::string(ptr, len);
        else
            return std::string("");
    }

    const char* ptr;
    size_t len;
};

struct StrHash {
    static const size_t prime = 1099511628211ULL;
    static const size_t seed = 14695981039346656037ULL;

    inline size_t fnv1a(unsigned char byte, size_t hash = seed) const {
        return (byte ^ hash) * prime;
    }

    size_t operator()(const Str& k) const {
        size_t h = fnv1a(1);
        for(int i = 0; i < k.len; ++i) {
            h = fnv1a((unsigned char)k.ptr[i], h);
        }
        return h;
    }
};
