#pragma once

#include "common.hpp"

#include <string>
#include <vector>

#include "error.hpp"
#include "str.hpp"

// Modify parser_conv_token too when changing this!
enum TokenType {
    TOK_WHITESPACE,
    TOK_NEWLINE,

    // Literals
    TOK_BOOL,
    TOK_INTEGER,
    TOK_FLOAT,
    TOK_STRING,
    TOK_IDENTIFIER,
    TOK_LABEL,

    // Keywords
    TOK_KEY_FUNC,
    TOK_KEY_STRUCT,
    TOK_KEY_RETURN,
    TOK_KEY_GOTO,
    TOK_KEY_FOR,
    TOK_KEY_WHILE,
    TOK_KEY_IF,
    TOK_KEY_ELSE,
    TOK_KEY_VAR,
    TOK_KEY_CONST,
    TOK_KEY_MODULE,
    TOK_KEY_USE,

    // Operators
    TOK_OP_ASSIGN,
    TOK_OP_EQ,
    TOK_OP_NOTEQ,
    TOK_OP_LESS,
    TOK_OP_LESSEQ,
    TOK_OP_MORE,
    TOK_OP_MOREEQ,
    TOK_OP_PLUS,
    TOK_OP_MINUS,
    TOK_OP_MUL,
    TOK_OP_DIV,
    TOK_OP_PLUSASSIGN,
    TOK_OP_MINUSASSIGN,
    TOK_OP_MULASSIGN,
    TOK_OP_DIVASSIGN,
    TOK_OP_PLUSPLUS,
    TOK_OP_MINUSMINUS,

    // Misc. syntax
    TOK_SYN_OPEN_BRACE,
    TOK_SYN_CLOSE_BRACE,
    TOK_SYN_OPEN_PAREN,
    TOK_SYN_CLOSE_PAREN,
    TOK_SYN_OPEN_BRACKET,
    TOK_SYN_CLOSE_BRACKET,
    TOK_SYN_DOT,
    TOK_SYN_COMMA,
    TOK_SYN_COLON,
    TOK_SYN_SEMICOLON,

    TOK_COUNT
};

struct Token {
    const TokenType type;
    const Str str;

    const int line, column;
    const char* file;

    bool parse_bool(bool& out) const;
    bool parse_int(int64& out) const;
    bool parse_uint(uint64& out) const;
    bool parse_float(float& out) const;
    bool parse_double(double& out) const;
    bool parse_string(std::string& out) const;

    Token(TokenType type, const Str& str);

    Token(TokenType type,
          const char* ts,
          const char* te,
          int line,
          int column,
          const char* file);

    Token(const Token& v) = default;
};

Error lex_scan(const char* in, const char* file, std::vector<Token>& out);
