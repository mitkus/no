# No

An experimental programming language. Will probably not work on this anymore, C++ is really icky for writing compiler code.

Code example:
```
func main(arg: str[]): i32 {
    var a: i32 = 2;
    var b: i32 = (a * 2) + 1;

    a = (b + a - 1) / 2;

    println("a = {}", a);
    println("b = {}", b);

    for(var i: int = 0; i < arg.length; ++i) {
        println("arg {} = {}", i, arg[i]);
    }

    return 0;
}
```

Uses [re2c](http://re2c.org/) for lexing and [lemon](http://www.hwaci.com/sw/lemon/) for parsing.