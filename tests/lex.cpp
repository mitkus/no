#include <vector>

#include "lex.hpp"

#include "utest.h"

UTEST(lex, errors) {
    std::vector<Token> out;

    ASSERT_TRUE(lex_scan("()", "", out).success());
    ASSERT_FALSE(lex_scan("\" \n", "", out).success());
}

UTEST(lex, string_success) {
    std::vector<Token> out;
    std::string str;
    ASSERT_TRUE(lex_scan("\"abcde\"", "", out).success());
    ASSERT_EQ(1, out.size());
    ASSERT_EQ(TOK_STRING, out[0].type);
    ASSERT_TRUE(out[0].parse_string(str));
    ASSERT_EQ("abcde", str);
}

UTEST(lex, string_fail) {
    std::vector<Token> out;
    ASSERT_FALSE(lex_scan("\"abcde", "", out).success());
    ASSERT_EQ(0, out.size());
}

UTEST(lex, identifiers) {
    std::vector<Token> out;
    ASSERT_TRUE(lex_scan("identifier", "", out).success());
    ASSERT_EQ(1, out.size());
    ASSERT_EQ(TOK_IDENTIFIER, out[0].type);
    ASSERT_TRUE(out[0].str == "identifier");
    out.clear();

    ASSERT_TRUE(lex_scan("_id1 _id2  _i\ni", "", out).success());
    ASSERT_EQ(4, out.size());

    for(int i = 0; i < 4; ++i) {
        ASSERT_EQ(TOK_IDENTIFIER, out[i].type);
    }

    ASSERT_TRUE(out[0].str == "_id1");
    ASSERT_TRUE(out[1].str == "_id2");
    ASSERT_TRUE(out[2].str == "_i");
    ASSERT_TRUE(out[3].str == "i");
}

UTEST(lex, identifier_from_keywords) {
    std::vector<Token> out;
    ASSERT_TRUE(lex_scan("varconst", "", out).success());
    ASSERT_EQ(1, out.size());

    ASSERT_EQ(TOK_IDENTIFIER, out[0].type);
    ASSERT_TRUE(out[0].str == "varconst");
}

UTEST(lex, keywords) {
    std::vector<Token> out;
    ASSERT_TRUE(lex_scan("struct func structfunc _if", "", out).success());
    ASSERT_EQ(4, out.size());

    ASSERT_EQ(TOK_KEY_STRUCT, out[0].type);
    ASSERT_EQ(TOK_KEY_FUNC, out[1].type);
    ASSERT_EQ(TOK_IDENTIFIER, out[2].type);
    ASSERT_EQ(TOK_IDENTIFIER, out[3].type);
}

UTEST(lex, scomment) {
    std::vector<Token> out;
    ASSERT_TRUE(lex_scan("//int func", "", out).success());
    ASSERT_EQ(0, out.size());

    ASSERT_TRUE(lex_scan("a // b\n c\n//d\n", "", out).success());
    ASSERT_EQ(2, out.size());
    ASSERT_EQ(TOK_IDENTIFIER, out[0].type);
    ASSERT_TRUE(out[0].str == "a");
    ASSERT_EQ(TOK_IDENTIFIER, out[1].type);
    ASSERT_TRUE(out[1].str == "c");
}

UTEST(lex, mcomment) {
    std::vector<Token> out;
    ASSERT_FALSE(lex_scan("/* comment", "", out).success());
    ASSERT_FALSE(lex_scan("/* comment*", "", out).success());
    ASSERT_EQ(0, out.size());

    ASSERT_TRUE(lex_scan(" /**//*\n\ntest*/a", "", out).success());
    ASSERT_EQ(1, out.size());
    ASSERT_EQ(TOK_IDENTIFIER, out[0].type);
    ASSERT_TRUE(out[0].str == "a");
}

UTEST(lex, integers) {
    int64 num = 0;
    std::vector<Token> out;
    ASSERT_TRUE(lex_scan("3", "", out).success());
    ASSERT_EQ(1, out.size());
    ASSERT_EQ(TOK_INTEGER, out[0].type);
    ASSERT_TRUE(out[0].parse_int(num));
    ASSERT_EQ(3, num);

    out.clear();
    int64 a = 0, b = 0, c = 0, d = 0, e = 0;
    ASSERT_TRUE(
        lex_scan("0b0110 -510 0x10\n0012345678  0xabc ", "", out).success());
    ASSERT_EQ(5, out.size());
    for(int i = 0; i < 5; ++i) {
        ASSERT_EQ(TOK_INTEGER, out[i].type);
    }

    ASSERT_TRUE(out[0].parse_int(a));
    ASSERT_EQ(6, a);

    ASSERT_TRUE(out[1].parse_int(b));
    ASSERT_EQ(-510, b);

    ASSERT_TRUE(out[2].parse_int(c));
    ASSERT_EQ(16, c);

    ASSERT_TRUE(out[3].parse_int(d));
    ASSERT_EQ(12345678LL, d);

    ASSERT_TRUE(out[4].parse_int(e));
    ASSERT_EQ(0xabc, e);
}

UTEST(lex, bools) {
    std::vector<Token> out;
    ASSERT_TRUE(lex_scan("true\tfalse\n true false", "", out).success());
    ASSERT_EQ(4, out.size());
    for(int i = 0; i < 4; ++i) {
        bool val = false;
        ASSERT_EQ(TOK_BOOL, out[i].type);
        ASSERT_TRUE(out[i].parse_bool(val));
        ASSERT_EQ((i % 2 == 0), val);
    }
}

UTEST(lex, grammar1) {
    std::vector<Token> out;
    ASSERT_TRUE(lex_scan("var b: i32=arg.length", "", out).success());
    ASSERT_EQ(8, out.size());

    ASSERT_EQ(TOK_KEY_VAR, out[0].type);

    ASSERT_EQ(TOK_IDENTIFIER, out[1].type);
    ASSERT_TRUE(out[1].str == "b");

    ASSERT_EQ(TOK_SYN_COLON, out[2].type);

    ASSERT_EQ(TOK_IDENTIFIER, out[3].type);
    ASSERT_TRUE(out[3].str == "i32");

    ASSERT_EQ(TOK_OP_ASSIGN, out[4].type);

    ASSERT_EQ(TOK_IDENTIFIER, out[5].type);
    ASSERT_TRUE(out[5].str == "arg");

    ASSERT_EQ(TOK_SYN_DOT, out[6].type);

    ASSERT_EQ(TOK_IDENTIFIER, out[7].type);
    ASSERT_TRUE(out[7].str == "length");
}

UTEST(lex, positions) {
    std::vector<Token> out;
    const char* prg = "a\n\nb //c\r\n  c\td\n/*comment */e/*\n\n */f";
    ASSERT_TRUE(lex_scan(prg, "", out).success());
    ASSERT_EQ(6, out.size());

    ASSERT_EQ(TOK_IDENTIFIER, out[0].type);
    ASSERT_TRUE(out[0].str == "a");
    ASSERT_EQ(1, out[0].line);
    ASSERT_EQ(0, out[0].column);

    ASSERT_EQ(TOK_IDENTIFIER, out[1].type);
    ASSERT_TRUE(out[1].str == "b");
    ASSERT_EQ(3, out[1].line);
    ASSERT_EQ(0, out[1].column);

    ASSERT_EQ(TOK_IDENTIFIER, out[2].type);
    ASSERT_TRUE(out[2].str == "c");
    ASSERT_EQ(4, out[2].line);
    ASSERT_EQ(2, out[2].column);

    ASSERT_EQ(TOK_IDENTIFIER, out[3].type);
    ASSERT_TRUE(out[3].str == "d");
    ASSERT_EQ(4, out[3].line);
    ASSERT_EQ(4, out[3].column);

    ASSERT_EQ(TOK_IDENTIFIER, out[4].type);
    ASSERT_TRUE(out[4].str == "e");
    ASSERT_EQ(5, out[4].line);
    ASSERT_EQ(12, out[4].column);

    ASSERT_EQ(TOK_IDENTIFIER, out[5].type);
    ASSERT_TRUE(out[5].str == "f");
    ASSERT_EQ(7, out[5].line);
    ASSERT_EQ(3, out[5].column);
}
