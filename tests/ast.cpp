#include "ast.hpp"

#include "utest.h"

UTEST(ast, simple_type) {
    SimpleType t(Str("i32"));
    ASSERT_EQ("i32", t.print());
    ASSERT_EQ(" i32", t.print(" "));
}

UTEST(ast, array_type) {
    SimpleType i32(Str("i32"));
    ArrayType arr1(&i32);
    ArrayType arr2(&i32, false, 16);
    ArrayType arr3(&arr1);

    ASSERT_EQ("i32[]", arr1.print());
    ASSERT_EQ("i32[16]", arr2.print());
    ASSERT_EQ(" i32[][]", arr3.print(" "));
}

UTEST(ast, func_type) {
    SimpleType i32(Str("i32"));
    SimpleType str(Str("str"));
    ArrayType str_arr(&str);
    SimpleType _bool(Str("bool"));

    FuncType f1({}, nullptr);
    FuncType f2({&i32}, &_bool);
    FuncType f3({&str_arr, &i32}, &i32);

    ASSERT_EQ("func()", f1.print());
    ASSERT_EQ("func(i32): bool", f2.print());
    ASSERT_EQ("func(str[], i32): i32", f3.print());
}

UTEST(ast, decl) {
    SimpleType i32(Str("i32"));
    Decl a(Str("a"), &i32, nullptr);

    SimpleType f32(Str("f32"));
    VarExpr expr_a(Str("a"));
    Decl b(Str("b"), &f32, &expr_a);

    SimpleType _bool(Str("bool"));
    ArrayType bool_arr(&_bool);
    Decl c(Str("cc"), &bool_arr, nullptr);

    ASSERT_EQ("var a: i32", a.print());
    ASSERT_EQ("var b: f32 = a", b.print());
    ASSERT_EQ("var cc: bool[]", c.print());
}

UTEST(ast, block) {
    Block empty({});

    SimpleType f32(Str("f32"));
    VarExpr expr_a(Str("a"));
    Decl b(Str("b"), &f32, &expr_a);

    SimpleType _bool(Str("bool"));
    ArrayType bool_arr(&_bool);
    Decl c(Str("cc"), &bool_arr, nullptr);

    Block block_a({&b, &c});
    Block block_b({&b});

    ASSERT_EQ("{}", empty.print());

    ASSERT_EQ("{\n    var b: f32 = a\n    var cc: bool[]\n}", block_a.print());
    ASSERT_EQ("    {\n        var b: f32 = a\n    }", block_b.print("    "));
}

UTEST(ast, forstmt) {
    Block empty({});

    SimpleType i32(Str("i32"));
    ConstExpr zero(Token(TOK_INTEGER, Str("0")));

    Decl init(Str("i"), &i32, &zero);

    VarExpr check_left(Str("i"));
    ConstExpr check_right(Token(TOK_INTEGER, Str("100")));
    BinOpExpr check(Token(TOK_OP_LESS, Str("<")), &check_left, &check_right);

    VarExpr update_lhs(Str("i"));
    VarExpr update_rhs_left(Str("i"));
    ConstExpr update_rhs_right(Token(TOK_INTEGER, Str("1")));
    BinOpExpr update_rhs(
        Token(TOK_OP_PLUS, Str("+")), &update_rhs_left, &update_rhs_right);
    AssignStmt update(Token(TOK_OP_ASSIGN, Str("=")), &update_lhs, &update_rhs);

    ForStmt for1(&init, nullptr, nullptr, &empty);
    ForStmt for2(&init, &check, nullptr, &empty);
    ForStmt for3(&init, &check, &update, &empty);

    ASSERT_EQ("for (var i: i32 = 0;;)\n{}", for1.print());
    ASSERT_EQ("for (var i: i32 = 0; i < 100;)\n{}", for2.print());
    ASSERT_EQ("for (var i: i32 = 0; i < 100; i = i + 1)\n{}", for3.print());
}

UTEST(ast, ifstmt) {
    Block empty({});

    VarExpr left(Str("a"));
    VarExpr right_left(Str("b"));
    ConstExpr right_right(Token(TOK_INTEGER, Str("12")));
    BinOpExpr right(Token(TOK_OP_MINUS, Str("-")), &right_left, &right_right);
    BinOpExpr check(Token(TOK_OP_EQ, Str("==")), &left, &right);

    IfStmt if1(&check, &empty, nullptr);
    IfStmt if2(&check, &empty, &empty);

    ASSERT_EQ("if (a == b - 12)\n    {}", if1.print());
    ASSERT_EQ("if (a == b - 12)\n    {}\nelse\n    {}", if2.print());
}

UTEST(ast, callexpr) {
    VarExpr a(Str("a"));
    VarExpr b(Str("b"));
    BinOpExpr plus(Token(TOK_OP_PLUS, Str("+")), &a, &b);

    CallExpr call1("func1", {});
    CallExpr call2("func2", {&a});
    CallExpr call3("func3", {&a, &b, &plus});

    ASSERT_EQ("func1()", call1.print());
    ASSERT_EQ("func2(a)", call2.print());
    ASSERT_EQ("func3(a, b, a + b)", call3.print());
}

UTEST(ast, indexexpr) {
    VarExpr a(Str("a"));
    VarExpr b(Str("b"));
    ConstExpr c(Token(TOK_INTEGER, Str("12")));

    ConstExpr i1(Token(TOK_INTEGER, Str("0")));
    BinOpExpr i2(Token(TOK_OP_PLUS, Str("+")), &b, &c);

    IndexExpr idx1(&a, &i1);
    IndexExpr idx2(&a, &i2);

    ASSERT_EQ("a[0]", idx1.print());
    ASSERT_EQ("a[b + 12]", idx2.print());
}

UTEST(ast, memberexpr) {
    VarExpr a(Str("a"));

    MemberExpr m1(&a, Str("length"));
    MemberExpr m2(&a, Str("pos"));

    ASSERT_EQ("a.length", m1.print());
    ASSERT_EQ("a.pos", m2.print());
}

UTEST(ast, funcexpr) {
    SimpleType i32(Str("i32"));
    SimpleType str(Str("str"));
    ArrayType str_arr(&str);
    FuncType ft({&str_arr, &i32}, &i32);

    VarExpr a(Str("a"));
    VarExpr b(Str("b"));
    BinOpExpr plus(Token(TOK_OP_PLUS, Str("+")), &a, &b);

    CallExpr call1("func1", {&a});
    CallExpr call2("func2", {&a, &b, &plus});

    Block block({&call1, &call2});

    FuncExpr func(&ft, "foo", {"a", "b"}, &block);

    ASSERT_EQ(
        "func foo(a: str[], b: i32): i32 {\n"
        "    func1(a)\n"
        "    func2(a, b, a + b)\n"
        "}",
        func.print());
}
