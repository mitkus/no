#include "linalloc.hpp"

#include "utest.h"

UTEST(linalloc, simple) {
    LinAlloc l;
    int32* a = l.alloc<int32>(-5);
    int64* b = l.alloc<int64>(1000000000000LL);
    bool* c = l.alloc<bool>(false);
    double* d = l.alloc<double>(3.1415);

    ASSERT_TRUE(-5 == *a);
    ASSERT_TRUE(1000000000000LL == *b);
    ASSERT_TRUE(false == *c);
    ASSERT_TRUE(3.1415 == *d);

    *a = 100;
    *c = true;

    ASSERT_TRUE(100 == *a);
    ASSERT_TRUE(1000000000000LL == *b);
    ASSERT_TRUE(true == *c);
    ASSERT_TRUE(3.1415 == *d);
}

const size_t n_stress_ptrs = 100000;
size_t* stress_ptrs[n_stress_ptrs];

UTEST(linalloc, stress) {
    LinAlloc l;

    for(size_t i = 0; i < n_stress_ptrs; ++i) {
        size_t* p = l.alloc(i * 1711083);
        stress_ptrs[i] = p;
    }

    for(size_t i = 0; i < n_stress_ptrs; ++i) {
        size_t* p = stress_ptrs[i];
        ASSERT_TRUE(i * 1711083 == *p);
    }
}
