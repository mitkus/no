#include "parser.hpp"
#include "lex.hpp"

#include "utest.h"

static std::vector<Token> tokenize(const char* prg) {
    std::vector<Token> tokens;
    Error err = lex_scan(prg, "", tokens);
    assert(err.success());
    return tokens;
}

UTEST(parser, empty_block) {
    LinAlloc alloc;
    Node* out;
    Error err = parser_parse(tokenize("{}"), &out, &alloc);
    ASSERT_TRUE(err.success());
    ASSERT_EQ("{\n    {}\n}", out->print());
}

UTEST(parser, bad_block) {
    LinAlloc alloc;
    Node* out;
    Error err = parser_parse(tokenize("{{"), &out, &alloc);
    ASSERT_FALSE(err.success());
    ASSERT_TRUE(out == nullptr);
}
