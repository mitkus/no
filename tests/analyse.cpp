#include "analyse.hpp"

#include "lex.hpp"
#include "parser.hpp"

#include "utest.h"

static Error compile(const char* src, LinAlloc* alloc) {
    const char* filename = "<test>";

    std::vector<Token> tokens;
    Error err = lex_scan(src, filename, tokens);
    if(!err.success()) return err;

    Node* ast;
    err = parser_parse(tokens, &ast, alloc, filename);
    if(!err.success()) return err;

    BlockInfoMap blmap;
    err = analyse_blocks(ast, alloc, blmap, filename);
    if(!err.success()) return err;

    return analyse_types(ast, alloc, blmap, filename);
}

UTEST(analyse, blocks_func) {
    LinAlloc alloc;

    const char* prg1 = "func f() {}";
    Error err = compile(prg1, &alloc);
    ASSERT_TRUE(err.success());

    const char* prg2 =
        "func f() {}\n"
        "func f() {}\n";
    err = compile(prg2, &alloc);
    ASSERT_EQ(ERR_ANALYSE_FUNC_ALREADY_DECLARED, err.type);
    ASSERT_EQ(2, err.line);
    ASSERT_EQ(5, err.column);
}

UTEST(analyse, blocks_var) {
    LinAlloc alloc;

    const char* prg1 =
        "var a: i32;\n"
        "var b: bool = false;\n"
        "func f(c: u64) {\n"
        "  var a: f32 = 0.0;\n"
        "  var d: i64 = 0;\n"
        "}";
    Error err = compile(prg1, &alloc);
    ASSERT_TRUE(err.success());

    // Same var name
    const char* prg2 =
        "var a: i32;\n"
        "var a: bool = false;\n";
    err = compile(prg2, &alloc);
    ASSERT_EQ(ERR_ANALYSE_VAR_ALREADY_DECLARED, err.type);
    ASSERT_EQ(2, err.line);
    ASSERT_EQ(4, err.column);

    // Same var name, same type
    const char* prg3 =
        "var a: i32;\n"
        "var a: i32;\n";
    err = compile(prg3, &alloc);
    ASSERT_EQ(ERR_ANALYSE_VAR_ALREADY_DECLARED, err.type);
    ASSERT_EQ(2, err.line);
    ASSERT_EQ(4, err.column);

    // Same var name as function argument
    const char* prg4 =
        "func f(a: i32) {\n"
        "  var b: bool = true;\n"
        "  var a: i32;\n"
        "}";
    err = compile(prg4, &alloc);
    ASSERT_EQ(ERR_ANALYSE_VAR_ALREADY_DECLARED, err.type);
    ASSERT_EQ(3, err.line);
    ASSERT_EQ(6, err.column);

    // Same var name, but shadowed in a new block
    const char* prg5 =
        "func f() {\n"
        "  var a: i64 = 5;\n"
        "  {\n"
        "    var a: bool = false;\n"
        "  }\n"
        "}";
    err = compile(prg5, &alloc);
    ASSERT_TRUE(err.success());
}

UTEST(analyse, type_mismatch) {
    LinAlloc alloc;

    // Assigning wrong type in decl
    const char* prg1 = "var a: bool = 0;\n";
    Error err = compile(prg1, &alloc);
    ASSERT_EQ(ERR_ANALYSE_TYPE_MISMATCH, err.type);
    ASSERT_EQ(1, err.line);
    ASSERT_EQ(4, err.column);

    // Passing wrong type to a function
    const char* prg2 =
        "func f(a: i64) { return; }\n"
        "f(3.14);";
    err = compile(prg2, &alloc);
    ASSERT_EQ(ERR_ANALYSE_TYPE_MISMATCH, err.type);
    ASSERT_EQ(2, err.line);
    ASSERT_EQ(1, err.column);

    // Adding two variables of different type
    const char* prg3 =
        "var a: i64 = 10;\n"
        "var b: i64 = a + 1;\n"
        "var c: f32 = 0.1;\n"
        "var d: f32 = a + b + c;\n";
    err = compile(prg3, &alloc);
    ASSERT_EQ(ERR_ANALYSE_TYPE_MISMATCH, err.type);
    ASSERT_EQ(4, err.line);
    ASSERT_EQ(19, err.column);

    // Use function with wrong return type
    const char* prg4 =
        "func one(): i64 { return 1; }\n"
        "var a: i64 = one();\n\n"
        "var b: i64 = a * one();\n"
        "var c: bool = one();\n";
    err = compile(prg4, &alloc);
    ASSERT_EQ(ERR_ANALYSE_TYPE_MISMATCH, err.type);
    ASSERT_EQ(5, err.line);
    ASSERT_EQ(4, err.column);
}

UTEST(analyse, use_undeclared_var) {
    LinAlloc alloc;

    // Using undeclared var in decl expression
    const char* prg1 = "var a: i32 = b;\n";
    Error err = compile(prg1, &alloc);
    ASSERT_EQ(ERR_ANALYSE_USE_UNDECLARED_VARIABLE, err.type);
    ASSERT_EQ(1, err.line);
    ASSERT_EQ(13, err.column);
}

UTEST(analyse, use_before_decl) {
    LinAlloc alloc;

    const char* prg1 = "var b: i64 = a + 1; var a: i64 = 2;";
    Error err = compile(prg1, &alloc);
    ASSERT_EQ(ERR_ANALYSE_USE_UNDECLARED_VARIABLE, err.type);
    ASSERT_EQ(1, err.line);
    ASSERT_EQ(13, err.column);
}

UTEST(analyse, use_before_shadowing) {
    LinAlloc alloc;

    // Use variable before shadowing it in current scope
    const char* prg1 =
        "func f(a: i64) {\n"
        "  var b: i64 = 2 * (a + 1);\n"
        "  {\n"
        "    var c: i64 = 10 + a + b;\n"
        "    var a: i64 = 0;\n"
        "  }\n"
        "}";
    Error err = compile(prg1, &alloc);
    ASSERT_EQ(ERR_ANALYSE_USE_BEFORE_SHADOWING, err.type);
    ASSERT_EQ(4, err.line);
    ASSERT_EQ(22, err.column);
}

UTEST(analyse, shadow_function) {
    LinAlloc alloc;

    // Shadow a function with different type function
    const char* prg1 =
        "func f(a: i64): bool { return true; }\n"
        "func main() {\n"
        "   var a: bool = f(1);\n"
        "   {\n"
        "       func f(a: i64): i64 { return a + 1; }\n"
        "       var b: i64 = f(3);\n"
        "   }\n"
        "}";

    Error err = compile(prg1, &alloc);
    ASSERT_TRUE(err.success());
}

UTEST(analyse, call_before_shadowing) {
    LinAlloc alloc;

    const char* prg1 =
        "func f(a: i64): i64 { return a + 2; }\n"
        "func main() {\n"
        "   var a: i64 = f(1);\n"
        "   {\n"
        "       var b: i64 = f(3);\n"
        "       func f(a: i64): i64 { return a + 1; }\n"
        "   }\n"
        "}";

    Error err = compile(prg1, &alloc);
    ASSERT_EQ(ERR_ANALYSE_CALL_BEFORE_SHADOWING, err.type);
    ASSERT_EQ(5, err.line);
    ASSERT_EQ(21, err.column);
}

UTEST(analyse, use_undeclared_function) {
    LinAlloc alloc;

    const char* prg1 =
        "func sqr(a: i64): i64 { return a * a; }\n"
        "func add(a: i64, b: i64): i64 { return a + a; }\n"
        "var a: i64 = sqr(2);\n"
        "var b: i64 = add(sqr(2), 11);\n"
        "var c: i64 = add(sqr(2), mul(11, 2));\n";
    Error err = compile(prg1, &alloc);
    ASSERT_EQ(ERR_ANALYSE_USE_UNDECLARED_FUNCTION, err.type);
    ASSERT_EQ(5, err.line);
    ASSERT_EQ(28, err.column);
}

UTEST(analyse, wrong_number_of_arguments) {
    LinAlloc alloc;

    // Pass not enough arguments
    const char* prg1 =
        "func sqr(a: i64): i64 { return a * a; }\n"
        "var a: i64 = sqr();\n";
    Error err = compile(prg1, &alloc);
    ASSERT_EQ(ERR_ANALYSE_WRONG_NUMBER_OF_ARGUMENTS, err.type);
    ASSERT_EQ(2, err.line);
    ASSERT_EQ(16, err.column);

    // Pass one argument when none needed
    const char* prg2 =
        "func one(): i64 { return 1; }\n"
        "var thiswillnotwork: i64 = one(40911);\n";
    err = compile(prg2, &alloc);
    ASSERT_EQ(ERR_ANALYSE_WRONG_NUMBER_OF_ARGUMENTS, err.type);
    ASSERT_EQ(2, err.line);
    ASSERT_EQ(30, err.column);

    // Pass two arguments when one needed
    const char* prg3 =
        "func sqr(a: i64): i64 { return a * a; }\n"
        "var boo: i64 = sqr(1, 1);\n";
    err = compile(prg3, &alloc);
    ASSERT_EQ(ERR_ANALYSE_WRONG_NUMBER_OF_ARGUMENTS, err.type);
    ASSERT_EQ(2, err.line);
    ASSERT_EQ(18, err.column);
}
