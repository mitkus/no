#include "strutil.hpp"

#include "utest.h"

UTEST(strutil, str_join) {
    ASSERT_EQ("", str_join({}));
    ASSERT_EQ("a", str_join({"a"}));
    ASSERT_EQ("12345", str_join({"", "1", "23", "4", "5"}));
}

UTEST(strutil, str_join_delim) {
    ASSERT_EQ("", str_join({}, ","));
    ASSERT_EQ("a", str_join({"a"}, ","));
    ASSERT_EQ("1, 23, 4, 5", str_join({"1", "23", "4", "5"}, ", "));
}
