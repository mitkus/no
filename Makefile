CXX=clang++
CXXFLAGS=-std=c++11 -g -Wall -Isrc -D_DEBUG
CXXFLAGS_TEST=$(CXXFLAGS) -fsanitize=address -DTEST

all: no

src/lex.cpp: src/lex.re
	re2c src/lex.re -o src/lex.cpp

src/grammar.cpp: src/grammar.y
	lemon -l src/grammar.y
	mv src/grammar.c src/grammar.cpp

.PHONY: test
test: tests/build.cpp src/build.cpp src/lex.cpp src/grammar.cpp
	$(CXX) $(CXXFLAGS_TEST) tests/build.cpp src/build.cpp -o test

.PHONY: t
t: test
	./test

.PHONY: no
no: src/build.cpp src/lex.cpp src/grammar.cpp
	$(CXX) $(CXXFLAGS) src/build.cpp -o no

clean:
	rm -f src/lex.cpp
	rm -f src/grammar.cpp
	rm -r -f $(ODIR)
	rm -r -f $(ODIR_TEST)
	rm -r -f *.dSYM
	rm -f no
	rm -f test

.PHONY: format
format:
	clang-format -i src/*.cpp
	clang-format -i src/*.hpp
	clang-format -i tests/*.cpp
